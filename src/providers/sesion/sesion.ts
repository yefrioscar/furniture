import { HttpClient, HttpResponseBase } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SesionProvider {

	BASEURL = 'https://furniture-api-node.herokuapp.com/api';
	BASEJAVA = 'http://45.55.79.245:8080/testUp/';

	constructor(public http: HttpClient) {

	}

	getUser() {
		let usuario =  JSON.parse(localStorage.getItem('usuario'));

		return usuario ? usuario : {};
	}


	iniciarSesion(email, clave) {
		return this.http.post(`${this.BASEURL}/iniciosesion`, {
			email: email,
			clave: clave
		})
			.do(this.logResponse)
			.map(this.extractData)
			.catch(this.catchError)
	}

	crearCuenta(email, clave, fecha, nombres, apellidos) {
		return this.http.post(`${this.BASEURL}/registro`, {
			usuario: email,
			contrasena: clave,
			sexo: "M",
			fechaNacimiento: fecha,
			nombres: nombres,
			apellidos: apellidos
		})
			.do(this.logResponse)
			.map(this.extractData)
			.catch(this.catchError)
	}

	verificarCuenta(email, code) {
		return this.http.post(`${this.BASEURL}/verificacioncorreo`, {
			email: email,
			codigoVerificacion: code
		})
			.do(this.logResponse)
			.map(this.extractData)
			.catch(this.catchError)
	}

	restrablecerContrasena(email) {
		return this.http.post(`${this.BASEURL}/cambiodecontrasena`, {
			email: email
		})
			.do(this.logResponse)
			.map(this.extractData)
			.catch(this.catchError)
	}

	cambiarContrasena(clave, nuevaclave) {
		return this.http.put(`${this.BASEJAVA}login/updatePassword`, {
			"idUsuario": this.getUser().idUsuario,
			"clave": clave,
			"nuevaClave": nuevaclave
		})
			.do(this.logResponse)
			.map(this.extractData)
			.catch(this.catchError)
	}

	actualizarUsuario(email, nombres, apellidos, fecha) {
		return this.http.put(`${this.BASEJAVA}login/updateUser`, {
			"email":email,
			"nombre":nombres,
			"apellido":apellidos,
			"fechaNacimiento":fecha,
			"idUsuario": this.getUser().idUsuario
		})
			.do(this.logResponse)
			.map(this.extractData)
			.catch(this.catchError)
	}

	private catchError(error: any) {
		console.log(error);

		return "E";
	}

	private logResponse(res: any) {
		console.log(res);
	}

	private extractData(res: any) {
		if (res.errors) {
			return res.errors[0].detail
		} else {
			return res
		}
	}

}
