import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Storage } from '@ionic/storage';
import { ProductosProvider } from '../../providers/productos/productos';
@Component({
  selector: 'page-detalle-compra',
  templateUrl: 'detalle-compra.html',
})

export class DetalleCompraPage {

  compra: any = {};



  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public _DomSanitizer: DomSanitizer, 
              private storage: Storage,
              private productos: ProductosProvider) {
    this.compra = this.navParams.get('compra');
    
    this.listarDetalle();
  }

  listarDetalle() {
    this.productos.listarDetalleVenta(this.compra.idVenta).subscribe( data =>{
      if(typeof data === 'string') {
        
      } else {
        this.compra.detalle = this.parArray(data);
      }

    });
  }

  parseDecimal(num) {
		return parseFloat((Math.round(num * 100) / 100).toString()).toFixed(2);
	}

  parArray(list) {
    
        for (let item of list) {
          item.precioParse = 'S/.' + this.parseDecimal(item.precio - (item.precio * (item.descuento / 100)));
          item.descuentoParse = '-' + item.descuento + '%';
          item.precioAnterior = 'S/.' + this.parseDecimal(item.precio);
          item.subtotalParse = 'S/.' + this.parseDecimal(item.subTotal);
        }
    
        return list;
      }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalleCompraPage');
  }

}
