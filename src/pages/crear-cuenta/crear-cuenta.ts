import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { SesionProvider } from '../../providers/sesion/sesion';

@Component({
	selector: 'page-crear-cuenta',
	templateUrl: 'crear-cuenta.html',
})
export class CrearCuentaPage {


	email: string;
	pass: string;
	nombres: string;
	apellidos: string;
	fecha: string;
	loading: any = this.loadingCtrl.create({ content: 'Cargando espere...' });
	

	errorMessage: string;

	constructor(public navCtrl: NavController, 
				public loadingCtrl: LoadingController, 
				public navParams: NavParams, 
				public viewCtrl: ViewController,
				public sesion: SesionProvider,
				public alertCtrl: AlertController) {

	}

	ionViewDidLoad() {
	}

	close() {
		this.viewCtrl.dismiss();
	}

	crear() {
		
		this.loading.present();
		this.sesion.crearCuenta(this.email, this.pass,this.fecha,this.nombres,this.apellidos).subscribe(
			data => {
				console.log(data);
				if (typeof data === 'string') {
					if(data.length === 1) this.errorMessage = "Server error";
					else this.errorMessage = data
				} else {		
					this.presentConfirm('Cuenta creada exitosamente. Se te envio un codigo de verificación a tu correo.');
				}

				this.loading.dismiss();
			}
		)
	}

	presentConfirm(mensaje) {
		let alert = this.alertCtrl.create({
		  title: 'Exito',
		  message: mensaje,
		  buttons: [
			{
			  text: 'Ok',
			  role: 'cancel',
			  handler: () => {
				this.viewCtrl.dismiss(this.email);				
			  }
			}
		  ]
		});
		alert.present();
	  }

}
