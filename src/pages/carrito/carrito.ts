import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { MetodoEnvioPage } from '../metodo-envio/metodo-envio';
import { IniciarSesionPage } from '../iniciar-sesion/iniciar-sesion';

import { Util } from '../../recursos/util';

@Component({
	selector: 'page-carrito',
	templateUrl: 'carrito.html',
})
export class CarritoPage {

	lista: any[];

	constructor(public navCtrl: NavController, public navParams: NavParams, public _DomSanitizer: DomSanitizer, public recursos: Util) {
		this.lista = JSON.parse(localStorage.getItem('productos')) ? JSON.parse(localStorage.getItem('productos')) : [];
		/*
			for (let item of this.lista) {
			  item.cantidad = 1;
			}
		*/

	}

	mov(item, tipo) {
		this.cerrarTodo();

		for (let e of this.lista) {
			e.idProducto === item.idProducto ? tipo === 1 ? e.cantidad-- : e.cantidad++ : false;
		}

		localStorage.setItem('productos', JSON.stringify(this.lista));
	}

	next() {
		let usuario = JSON.parse(localStorage.getItem('usuario'));
		

		if(usuario) {
			this.navCtrl.push(MetodoEnvioPage, {
			}, );
		} else {
			this.navCtrl.push(IniciarSesionPage, {
				callback: this.myCallbackFunction
			}, );
		}

	}

	myCallbackFunction = (_params) => {
		return new Promise((resolve, reject) => {
			this.validarUsuario();
			resolve();
		});
	}

	validarUsuario() {
		if (this.validarSesion()) {
			this.navCtrl.push(MetodoEnvioPage, {
			}, );;
		} else {
			
		}
	}

	validarSesion() {
		let usuario = JSON.parse(localStorage.getItem('usuario'));
		let token = localStorage.getItem('token');

		return usuario && token ? true : false;
	}

	cerrarTodo() {
		for(let e of this.lista) {
			e.tab = false;	
		}
	}

	tapEvent(e,item) {
		this.cerrarTodo();

		item.tab = true;
	}

	taB(e) {

	}

	delete(item) {
		this.recursos.eliminarProducto(item);

		let e = this.lista.indexOf(item);
		this.lista.splice(e,1);
	}	
}
