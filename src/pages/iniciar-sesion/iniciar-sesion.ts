import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { SesionProvider } from '../../providers/sesion/sesion';
import { VerificarCuentaPage } from '../verificar-cuenta/verificar-cuenta';
import { CrearCuentaPage } from '../crear-cuenta/crear-cuenta';
import { CambiarContrasenaPage } from '../cambiar-contrasena/cambiar-contrasena';
@Component({
	selector: 'page-iniciar-sesion',
	templateUrl: 'iniciar-sesion.html',
})
export class IniciarSesionPage {


	user: any = false;
	email: string;
	pass: string;
	loading: any;

	errorMessage: string;
	callback: any;

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public modalCtrl: ModalController,
		public viewCtrl: ViewController,
		public sesion: SesionProvider,
		public loadingCtrl: LoadingController,
		public alertCtrl: AlertController) {
			this.callback = this.navParams.get('callback');
			

	}
	ionViewDidLeave() {
		this.callback(this.user).then(() => { });
	}



	iniciarSesion() {

		this.presentLoading();
		this.sesion.iniciarSesion(this.email, this.pass).subscribe(
			data => {
				console.log(data);
				if (typeof data === 'string') {
					if(data.length === 1) this.presentConfirm('Server error');
					else this.presentConfirm(data);
				} else {	
					localStorage.setItem('token', data.result.token);
					localStorage.setItem('usuario', JSON.stringify(data.result.user));
					this.user = true;
					this.navCtrl.pop();
				}

				this.loading.dismiss();
			}
		)
	}

	verificarCuenta() {
		let profileModal = this.modalCtrl.create(VerificarCuentaPage, {
			email: this.email
		  });

		profileModal.onDidDismiss(data => {
			if (data) this.email = data
		});

		profileModal.present();
	}

	creaCuenta() {
		let profileModal = this.modalCtrl.create(CrearCuentaPage, { });

		profileModal.onDidDismiss(data => {
			if (data) {
				this.email = data
				this.verificarCuenta();
			} 
		});

		profileModal.present();
	}
	
	goCambiar() {

		let profileModal = this.modalCtrl.create(CambiarContrasenaPage, { });

		profileModal.onDidDismiss(data => {
			if (data) {
				this.email = data
			} 
		});

		profileModal.present();
	}

	presentConfirm(mensaje) {
		let alert = this.alertCtrl.create({
		  title: 'Sucedio algo',
		  message: mensaje,
		  buttons: [
			{
			  text: 'Ok',
			  role: 'cancel',
			  handler: () => {
			  }
			}
		  ]
		});
		alert.present();
	  }

	   presentLoading() {
		this.loading = this.loadingCtrl.create({ content: 'Cargando espere...' });
		this.loading.present();
		
	   }
}


