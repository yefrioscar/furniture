import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController, Slides, NavParams } from 'ionic-angular';
import { DetailsPage } from '../details/details';
import { IniciarSesionPage } from '../iniciar-sesion/iniciar-sesion';
import { PerfilPage } from '../perfil/perfil';
import { CarritoPage } from '../carrito/carrito';
import { ProductosProvider } from '../../providers/productos/productos';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ComprasPage } from '../compras/compras';
import { Util } from '../../recursos/util';
import { ChatPage } from '../chat/chat';
@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})

export class HomePage {

	listDefault: any = [0, 1, 2, 3];

	listaNow: any[];
	errorNow: string;

	listaPopular: any[];
	errorPopular: string;

	listaSale: any[];
	errorSale: string;

	logIn = false;

	lista = [];

	msmBot: string;

	@ViewChild('slides') slides: Slides;

	constructor(public navCtrl: NavController,
		public modalCtrl: ModalController,
		public navParams: NavParams,
		private productos: ProductosProvider,
		public _DomSanitizer: DomSanitizer,
		public recursos: Util) {

		let where = this.navParams.get('params');
		if (where) this[where]();

		this.validarUsuario();
		this.listarForNow();
		this.listarForPopular();
		this.listarForSale();
		this.listarMensajes();
	}

	listarMensajes() {
	 this.productos.getMessageRandom().subscribe( data => {
			if (typeof data === 'string') {
				this.lista = [];
			} else {
				this.lista = data.result;
				this.msmBot = this.lista[this.getRandomArbitrary(0, this.lista.length)];				
			}
		});
	}

	goCompras() {
		this.navCtrl.push(ComprasPage, {

		}, );

		this.recursos.eliminaCarrito();
	}


	getRandomArbitrary(min, max) {
		return Math.floor(Math.random() * (max - min)) + min;
	}

	myCallbackFunction = (_params) => {
		return new Promise((resolve, reject) => {
			this.validarUsuario();
			resolve();
		});
	}

	validarUsuario() {
		if (this.validarSesion()) {
			this.logIn = true;
		} else {
			this.logIn = false;
		}
	}

	listarForPopular() {
		this.productos.listarForPopular().subscribe(
			data => {
				if (typeof data === 'string') {
					this.errorPopular = 'Server error';
					this.listaPopular = [];
				} else {
					this.listaPopular = this.parArray(data);
				}
			}
		)
	}

	listarForNow() {
		this.productos.listarForNow().subscribe(
			data => {
				if (typeof data === 'string') {
					this.errorNow = 'Server error';
					this.listaNow = [];
				} else {
					this.listaNow = this.parArray(data);
				}
			}
		)
	}

	listarForSale() {
		this.productos.listarForSale().subscribe(
			data => {
				if (typeof data === 'string') {
					this.errorSale = 'Server error';
					this.listaSale = [];
				} else {
					this.listaSale = this.parArray(data);
				}
			}
		)
	}

	parArray(list) {

		for (let item of list) {
			item.precioParse = 'S/.' + this.parseDecimal(item.precio - (item.precio * (item.descuento / 100)));
			item.descuentoParse = '-' + item.descuento + '%';
			item.precioAnterior = 'S/.' + this.parseDecimal(item.precio);
		}

		return list;
	}

	parseDecimal(num) {
		return parseFloat((Math.round(num * 100) / 100).toString()).toFixed(2);
	}

	validarSesion() {
		let usuario = JSON.parse(localStorage.getItem('usuario'));
		let token = localStorage.getItem('token');

		return usuario && token ? true : false;
	}

	goDetails(item) {
		this.navCtrl.push(DetailsPage, {
			item: item
		}, );
	}

	goIniciarSesion() {
		this.navCtrl.push(IniciarSesionPage, {
			callback: this.myCallbackFunction
		}, );
	}

	goPerfil() {
		this.navCtrl.push(PerfilPage, {
			callback: this.myCallbackFunction
		}, );
	}

	bag() {
		this.navCtrl.push(CarritoPage, {

		}, );
	}


	goChat() {
		this.navCtrl.push(ChatPage, {
			message: this.msmBot
		}, );
	}

	doRefresh(refresher) {
		this.listarMensajes();

		setTimeout(() => {
		  refresher.complete();
		}, 2000);
	  }


}
