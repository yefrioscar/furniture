import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { SesionProvider } from '../../providers/sesion/sesion';
@Component({
	selector: 'page-cambiar-contrasena',
	templateUrl: 'cambiar-contrasena.html',
})

export class CambiarContrasenaPage {

	pa: string;
	pn: string;
	loading: any;

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		private sesion: SesionProvider,
		private alertCtrl: AlertController,
		private loadingCtrl: LoadingController) {
	}


	presentLoading() {
		this.loading = this.loadingCtrl.create({ content: 'Cargando espere...' });
		this.loading.present();
	}

	cambiarContrasena() {
		if (this.pa === this.pn) {
			this.presentAlert('No puede usar la misma contraseña.');
			return;
		}

		this.presentLoading();
		this.sesion.cambiarContrasena(this.pa, this.pn).subscribe(data => {
			if (typeof data === 'string') {
				this.presentAlert('No se pudo cambiar la contraseña. Contraseña antigua invalida.');
				this.pa = '';
				this.pn = '';
			} else {
				console.log(data);
				this.presentAlert('Se cambió la contraseña exitosamente.', true);
			}

			this.loading.dismiss();
		});
	}

	presentAlert(mensaje, val = false) {
		let alert = this.alertCtrl.create({
			title: !val ? 'Sucedió algo..': 'Exito',
			subTitle: mensaje,
			buttons: [{
				text: 'Ok',
				role: 'cancel',
				handler: () => {
					if (val) this.navCtrl.pop();
				}
			}]
		});
		alert.present();
	}

}
