import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';


@Component({
	selector: 'page-details',
	templateUrl: 'details.html',
})

export class DetailsPage {

	producto: any = {};
	productos: any[];
	textTipoBtn = 'Agregar al carro';

	constructor(public navCtrl: NavController, public navParams: NavParams, public _DomSanitizer: DomSanitizer, ) {
		this.producto = this.navParams.get('item');

		let producto = this.obtenerProducto(this.producto);

		console.log(producto);

		if (producto){
			this.textTipoBtn = 'Quitar del carro';
			this.producto = this.obtenerProducto(this.producto);
			this.producto.stockActual = +this.producto.stockActual;			
		} else {
			this.producto.cantidad = 1;
			this.producto.stockActual = +this.producto.stockActual;
		}  	

		this.producto.precioParse = 'S/.'+this.parseDecimal(this.producto.precio-(this.producto.precio*(this.producto.descuento/100)));
		this.producto.descuentoParse = '-'+this.producto.descuento+'%';
		this.producto.precioAnterior = 'S/.'+this.parseDecimal(this.producto.precio);
	}


	parseDecimal(num) {
		return parseFloat((Math.round(num * 100) / 100).toString()).toFixed(2);
    }

	obtenerListaProductos() {
		let pro = localStorage.getItem('productos');
		let lista = pro ? JSON.parse(pro) : null;

		return lista;
	}

	obtenerProducto(item) {
		let productos = this.obtenerListaProductos();
		let producto = productos ? productos.filter(item => item.idProducto === this.producto.idProducto)[0] : null;
		return producto ? producto : null;
	}

	eliminarProducto(item) {
		let productos: any[] = this.obtenerListaProductos();

		for (let e of productos) {
			if (item.idProducto === e.idProducto) {
				let a = productos.indexOf(e);
				productos.splice(a, 1);
			}
		}


		localStorage.setItem('productos', JSON.stringify(productos));
	}

	agregarProducto(item) {
		let productos = this.obtenerListaProductos();
		productos.push(item);
		localStorage.setItem('productos', JSON.stringify(productos));
	}

	actualizarProducto(item) {
		let productos: any[] = this.obtenerListaProductos();

		for (let e of productos) {
			if (item.idProducto === e.idProducto) {
				let a = productos.indexOf(e);
				productos[a] = item;
			}
		}

		localStorage.setItem('productos', JSON.stringify(productos));
	}

	addTocar() {
		this.productos = this.obtenerListaProductos();

		if (this.productos) {
			let producto = this.obtenerProducto(this.producto);

			if (this.textTipoBtn === 'Quitar del carro') {
				this.eliminarProducto(this.producto);
				this.textTipoBtn = 'Agregar al carro';
			} else {
				this.agregarProducto(this.producto);
				this.textTipoBtn = 'Quitar del carro';
			}

		} else {
			let lista = [];
			lista.push(this.producto);
			this.textTipoBtn = 'Quitar del carro';
			localStorage.setItem('productos', JSON.stringify(lista));
		}
	}

	mov(tipo) {
		if (this.textTipoBtn === 'Quitar del carro') {
			tipo === 1 ? this.producto.cantidad-- : this.producto.cantidad++;
			this.actualizarProducto(this.producto);
		} else {
			tipo === 1 ? this.producto.cantidad-- : this.producto.cantidad++;
		}
	}

}
